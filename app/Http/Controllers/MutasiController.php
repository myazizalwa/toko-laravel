<?php

namespace App\Http\Controllers;

use App\Pesanan;
use Cekmutasi;
use Illuminate\Http\Request;


class MutasiController extends Controller
{
    public function handleCallback(Request $request)
	{
        $ipn = Cekmutasi::catchIPN($request);
        
        if ($ipn->action == "payment_report") {
            foreach( $ipn->content->data as $data )
            {
                $amount = $data->amount;
                // $description = $data->description;
                $type =  $data->type;
                $angkaunik = substr($amount,-6);
                $harga = substr($amount,0, -6);
                $total = $harga."000";

                $pesanan = Pesanan::where('status', 1)
                            ->where('kode',$angkaunik)
                            ->where('jumlah_harga', $total)->latest(); // nanti disamakan dengan amount 
                            // dari cekmutasi

                if( $type == "credit") // dana masuk
                {
                    $pesanan->update([
                        'status' => 2
                    ]);
                }

                // echo " Angka Uniknya :".$angkaunik."</br> 
                // Harga :".$harga."</br>
                // Total :".$total;
                // dd($angkaunik.$total);
                // Bisa ditambah hit telegram API/ kirim email notifikasi
            };
        }
	}
}
